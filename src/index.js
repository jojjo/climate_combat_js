import 'rxjs';
import * as Observable from "rxjs";
import {Subscription} from "rxjs";
import { filter } from 'rxjs/operators';

export var INTERVAL = 1000;
export var game = {
    timer: Observable.interval(INTERVAL),
    subscription: Subscription,
    counter: 0,
    energy:  [{name: "Completely transfer to 100% renewable energy by 2030", value: 20}, {name: "Transfer to 50% renewable energy by 2030", value: 10}, {name: "No action towards transfer to renewable energy", value: 0}],
    politics: [{name: "Active Climate Politics", value: 20}, {name: "Moderate Climate Politics", value: 10}, {name: "Business as usual", value: 0}],
    politicalScore: 0,
    technology: [{name: "Large investments in research and development of renewables", value: 20}, {name: "Moderate investments in research and development of renewables", value: 10}, {name: "No investments in research and development of renewables", value: 0}],
    techScore: 0,
    totalScore: 0,
    transport: 0,
    currentTemperature: 15,
    temperature: 0,
    rcpTemps: [4, 6, 8],
    rcpTemp2: "1-4C",
    currentRcp: 2,
    seaLevel: 0,
    rcpSeaLevels: [0.45, 0.63, 1.0],
    co2: 0,
    rcpCo2: [475, 630, 1313],
    effects: 0,
    currentCo2: 400,
    solar:0,
    wind: 0,
    wave: 0,
    year: 2019,
    startYear: 2019,
    endYear: 2100,
    running:false,
    solar$:Observable,
    solarSubscription: Subscription,
    wind$: Observable,
    windSubscription: Subscription,
    wave$: Observable,
    waveSubscription: Subscription,
    politicsIndex: 2,
    energyIndex: 2,
    technologyIndex: 2,

    getInfo: function () {
        this.solar$ = Observable.of(this.solar);
        return 'counter=' + this.counter;
    },

    incrementCounter: function () {
        this.counter++;
        this.year++;
    },

    startTimer: function () {
        if (!this.running) {
            this.subscription = this.timer.subscribe((elapsed) => {
                this.incrementCounter();
                this.gameLoop();
                console.log(this.counter);
            });
            this.running = true;
        }
    },

    stopTimer: function () {
        if (this.running) {
            this.subscription.unsubscribe();
            this.running = false;
        }
    },

    updateClimate: function (){
        console.log("totalScore=" + this.totalScore + ", temperature=" + this.temperature + ", this.endYear - this.year=" + (this.endYear - this.startYear));
        if (this.totalScore >= 50) {
            if (this.temperature <= this.rcpTemps[0]) {
                this.temperature = this.temperature + (this.rcpTemps[0] / (this.endYear - this.startYear));
                console.log("Congratulations! You have achieved a total score of " + this.totalScore + " and an RCP of 2.4");
            }
            if (this.co2 <= this.rcpCo2[0]) {
                this.co2 = this.co2 + (this.rcpCo2[0] / (this.endYear - this.startYear));
            }
            if (this.seaLevel <= this.rcpSeaLevels[0]) {
                this.seaLevel = this.seaLevel + (this.rcpSeaLevels[0] / (this.endYear - this.startYear));
            }
            this.effects = "5% species loss, disruption to food chain on land and sea, more severe droughts and floods, limited economic losses, But with these goals we can still avoid disaster";
        }
        else if (this.totalScore < 50 && this.totalScore > 10) {
            if (this.temperature <= this.rcpTemps[1]) {
                this.temperature = this.temperature + (this.rcpTemps[1] / (this.endYear - this.startYear));
                console.log("Not looking good! You have achieved a total score of " + this.totalScore + " and an RCP of 4.6");
            }
            if (this.co2 <= this.rcpCo2[1]) {
                this.co2 = this.co2 + (this.rcpCo2[1] / (this.endYear - this.startYear));
            }
            if (this.seaLevel <= this.rcpSeaLevels[1]) {
                this.seaLevel = this.seaLevel + (this.rcpSeaLevels[1] / (this.endYear - this.startYear));
            }
            this.effects = "20% species loss, increased spread of infectious diseases, disruption to food chain on land and sea, more severe droughts and floods, 3% decrease of GDP globally ";
        }
        else if (this.totalScore <= 10) {
            if (this.temperature <= this.rcpTemps[2]) {
                this.temperature = this.temperature + (this.rcpTemps[2] / (this.endYear - this.startYear));
                console.log("Disaster! You have achieved a total score of " + this.totalScore + " and an RCP of 8.5");
            }
            if (this.co2 <= this.rcpCo2[2]) {
                this.co2 = this.co2 + (this.rcpCo2[2] / (this.endYear - this.startYear));
            }
            if (this.seaLevel <= this.rcpSeaLevels[2]) {
                this.seaLevel = this.seaLevel + (this.rcpSeaLevels[2] / (this.endYear - this.startYear));
            }
            this.effects = "40% species loss, reduced water supplies, failed crops, famine, riots and conflicts, migration, disruption to food chain on land and sea, more severe droughts and floods, 5% decrease of GDP globally";
        }
    },

    gameLoop: function () {
        this.updateClimate();
        if (this.isAlive()) {
            if (this.politics[this.politicsIndex].value >= 10) {
                console.log("You may add x more renewable resources");
            }
        }
        if (this.year >= this.endYear) {
            this.stopTimer();
        }
    },

    isAlive: function () {
        if (this.co2 > 1300) {
            console.log("Sad :(");
            this.stopTimer();
            return false;
        }
        return true;
    },

    allocateResources: function () {
        //let politicalScore = this.politics;
        //let techScore = this.technology;
        this.totalScore = this.technology[this.technologyIndex].value + this.politics[this.politicsIndex].value + this.energy[this.energyIndex].value;
        console.log("The current Political score is " + this.politics[this.politicsIndex].value);
        console.log("The current Technological score is " + this.technology[this.technologyIndex].value);
        console.log("The current Total score is " + this.totalScore);

    },

    demo: function() {
        this.solarSubscription = this.solar$.subscribe((result) =>
            console.log("!!UPDATE!! My Result is " + result), (error => console.error("FUCKED " + error)));
    },

    addSolar: function () {
        Observable.of(this.solar).pipe(filter(num => this.totalScore >= 20 && num < 15)).subscribe(_ => this.solar++);
        Observable.of(this.solar).pipe(filter(num => this.totalScore >= 10 && this.totalScore < 20 && num < 10)).subscribe(_ => this.solar++);
        Observable.of(this.solar).pipe(filter(num => this.totalScore === 2 && num < 5)).subscribe(_ => this.solar++);
    },
    removeSolar: function () {
        Observable.of(this.solar).pipe(filter(num => num !== 0)).subscribe(_ => this.solar--);
    },
    addWind: function () {
        Observable.of(this.wind).pipe(filter(num => this.totalScore >= 20 && num < 15)).subscribe(_ => this.wind++);
        Observable.of(this.wind).pipe(filter(num => this.totalScore >= 10 && this.totalScore < 20 && num < 10)).subscribe(_ => this.wind++);
        Observable.of(this.wind).pipe(filter(num => this.totalScore === 2 && num < 5)).subscribe(_ => this.wind++);
    },
    removeWind: function () {
        Observable.of(this.wind).pipe(filter(num => num !== 0)).subscribe(_ => this.wind--);
    },
    addWave: function () {
        Observable.of(this.wave).pipe(filter(num => this.totalScore >= 20 && num < 15)).subscribe(_ => this.wave++);
        Observable.of(this.wave).pipe(filter(num => this.totalScore >= 10 && this.totalScore < 20 && num < 10)).subscribe(_ => this.wave++);
        Observable.of(this.wave).pipe(filter(num => this.totalScore === 2 && num < 5)).subscribe(_ => this.wave++);
    },
    removeWave: function () {
        Observable.of(this.wave).pipe(filter(num => num !== 0)).subscribe(_ => this.wave--);
    },
    setPoliticalChoice: function (val) {
        this.politicsIndex = val;
    },
    getPoliticalChoice: function () {
        return this.politics[this.politicsIndex].name;
    },
    getPoliticalChoiceValue: function () {
        return this.politics[this.politicsIndex].value;
    },
    setTechInvestment: function (val) {
        this.technologyIndex = val;
    },
    getTechInvestment: function () {
        return this.technology[this.technologyIndex].name;
    },
    getTechInvestmentValue: function () {
        return this.technology[this.technologyIndex].value;
    },
    setEnergyChoice: function (val) {
        this.energyIndex = val;
    },
    getEnergyChoice: function () {
        return this.energy[this.energyIndex].name;
    },
    getEnergyChoiceValue: function () {
        return this.energy[this.energyIndex].value;
    }
};